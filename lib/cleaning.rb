require 'pathname'
module Cleaning
  CONFIG_FILE_VAR = 'CONFIG_FILE'
  def self.locations everywhere
    home_dir = Pathname.new(ENV['HOME'])
    if ENV.has_key? CONFIG_FILE_VAR
      config_file = Pathname.new(ENV[CONFIG_FILE_VAR])
    else
      config_file = home_dir/".banish.yml"
    end
    locations = `git remote`.split
    if config_file.exist?
      require 'yaml'
      config_locations = YAML.load IO::read(config_file)
      other_locations = locations - config_locations
      if everywhere
        config_locations + other_locations
      else
        config_locations
      end
    else
      locations
    end
  end
end
