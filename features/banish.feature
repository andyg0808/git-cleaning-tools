Feature: Basic banish
  As a user, I want to run `banish`
  In order to get rid of unneeded data.

  Background:
    Given a mocked home directory
    And a directory named "example_repo"
    And a directory named "example_remote"
    And a directory named "extra_remote"
    And I cd to "example_repo"
    And I successfully run `git init`
    And I successfully run `git annex init example`
    And I successfully run `git annex initremote remote type=directory directory=../example_remote encryption=none`
    And I successfully run `git annex initremote extr type=directory directory=../extra_remote encryption=none`
    Given an empty file named "repo_file"
    And I successfully run `git annex add repo_file`

  Scenario: Run completes
    When I run `banish .`
    Then it should pass with "drop repo_file ok"

  Scenario: Config file present
    Given a file named "~/.banish.yml" with:
    """
    - remote
    """
    When I run `banish .`
    Then it should pass with "drop repo_file ok"

  Scenario: Custom config file path
    Given a file named "~/banish.yml" with:
    """
    - remote
    """
    And I set the environment variable "CONFIG_FILE" to "~/banish.yml"
    When I run `banish .`
    Then it should pass with "drop repo_file ok"

  Scenario: Limit repos with config file
    Given I successfully run `git annex numcopies 2`
    And a file named "~/.banish.yml" with:
    """
    - remote
    """
    When I run `banish .`
    Then it should fail with "failed"

  Scenario: Multiple repos in config file
    Given I successfully run `git annex numcopies 2`
    And a file named "~/.banish.yml" with:
    """
    - remote
    - extr
    """
    When I run `banish .`
    Then it should pass with "drop repo_file ok"

  # Scenario: Config file present
  #   Given a file named "~/.banish.yml" with:
  #   """
  #   - remote
  #   """
  #   When I run `banish .`
  #   Then it should pass with "drop repo_file ok"
